var Request = require('request');
var Enumerable = require('linq');
var When = require('when');
var XPath = require('xpath');
var DOM = require('xmldom').DOMParser


module.exports = {
    onRun : function(config, dependencies, job_callback) {  
        var finalised = false;
        When.promise(function(resolve, reject) {
            var maxBuilds = config.maxBuilds ? config.maxBuilds : 20;
            var options = {                
                url: config.teamcity.apiEndpoint + '/builds?locator=buildType:' + config.buildType + ',count:' + maxBuilds,
                headers: {
                    'Accept': 'application/json'
                },
                auth: {
                    user: config.teamcity.username,
                    pass: config.teamcity.password,
                    sendImmediately: false
                }
            };
                    
            Request.get(options, function(error, response, body) {
                    if (error) {
                        reject(error);
                    }
                    
                    var response = JSON.parse(body);
                    resolve(response.build);
                }
            ); 
        })
        .then(function(builds) {
            if (finalised)
                return;

            var operations = Enumerable.from(builds)
                                       .select(function(build) {
                                           return When.promise(function(resolve, reject) {
                                               var options = {
                                                    url: config.teamcity.apiEndpoint + '/builds/id:' + build.id,                                                    
                                                    auth: {
                                                        user: config.teamcity.username,
                                                        pass: config.teamcity.password,
                                                        sendImmediately: false
                                                    }
                                                };
                                                        
                                                Request.get(options, function(error, response, body) {
                                                        if (error) {
                                                            reject(error);
                                                            return;
                                                        }

                                                        resolve(body);
                                                    }
                                                ); 
                                           });
                                       })
                                       .toArray();

            return When.all(operations);
        })
        .then(function(builds) {            
            var filterPattern = new RegExp(config.filter.pattern);
            var buildNumberPattern = new RegExp(config.buildNumber.pattern);            
            var resolved = false;

            builds.forEach(function(build) {
                if (resolved)
                    return;

                var buildXml = new DOM().parseFromString(build);                

                var filter = XPath.select1(config.filter.xpath, buildXml);                
                if (!filterPattern.test(filter.value))
                    return;

                var buildNumber = XPath.select1(config.buildNumber.xpath, buildXml);                
                var buildNumberMatches = buildNumber.value.match(buildNumberPattern);
                if (buildNumberMatches == null) {
                    throw 'Unable to match build number: ' + buildNumber.value + ' using pattern: ' + config.buildNumber.pattern;
                }

                var status = XPath.select1('/build/@status', buildXml);                   
                var buildPassed = status.value == 'SUCCESS';                
                
                job_callback(null, { VisualConfig : config.visualConfig, BuildNumber : buildNumberMatches[0], BuildPassed : buildPassed });
                resolved = true;
            });

            if (!resolved) {                
                dependencies.logger.warn('Unable to locate a matching build');
                job_callback(null, { VisualConfig : config.visualConfig });
            }
        })
        .timeout(60000)
        .catch(function(error) {         
            dependencies.logger.error('Error getting build number');
            finalised = true;
            job_callback(error);
        });        
    }
}