var Request = require('request');
var Enumerable = require('linq');
var When = require('when');
var Semver = require('semver');
var SemverUtils = require('semver-extra');

module.exports = {
    onRun : function(config, dependencies, job_callback) {        
        var operations = Enumerable.from(config.buildTypes)
                                   .select(function(buildType) {
                                        return When.promise(function(resolve, reject) {
                                            Request.get(config.teamcity.apiEndpoint + '/builds/buildType:' + buildType + ',branch:(default:any)/number', 
                                                {
                                                    auth: {
                                                        user: config.teamcity.username,
                                                        pass: config.teamcity.password,
                                                        sendImmediately: false
                                                    }
                                                },
                                                function(error, response, body) {
                                                    if (error) {
                                                        reject(error);
                                                        return;
                                                    }

                                                    resolve(body);
                                                }
                                            ); 
                                        });  
                                    })
                                    .toArray();

        When.all(operations)
            .then(function(versions) {
                var maxVersion = SemverUtils.max(versions);
                var releaseNumber = Semver.parse(maxVersion);
                releaseNumber.isPreRelease = SemverUtils.isPrerelease(maxVersion);
                job_callback(null, { VisualConfig : config.visualConfig, Release : releaseNumber });
            })
            .timeout(60000)
            .catch(function(error) {         
                dependencies.logger.error('Error getting build number');
                finalised = true;
                job_callback(error);
            });        
    }
}