widget = {
    onData : function(widget, data) {
        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
        }

        var releaseNumber = data.Release.major + '.' + data.Release.minor + '.' + data.Release.patch;
        var numberElement = $('div.value', widget);
        numberElement.text(releaseNumber);
        numberElement.css('color', data.VisualConfig.colour);

        var preReleaseElement = $('div.unit', widget);
        preReleaseElement.text(data.Release.isPreRelease ? 'pre-release' : 'release');
    },

    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title); 
    }
}