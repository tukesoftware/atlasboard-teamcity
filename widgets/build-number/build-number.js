widget = {
    onData : function(widget, data) {
        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
        }        
        
        var numberElement = $('div.value', widget);                
        numberElement.css('color', data.VisualConfig.colour);       

        if (!data.BuildNumber) {
            return;
        }

        numberElement.text(data.BuildNumber);

        if (data.VisualConfig.fontSize) {
            numberElement.css('font-size', data.VisualConfig.fontSize);       
        }

        if (data.VisualConfig.hideBuildStatus)
            return;

        var testStatusElement = $('canvas.build-status', widget).get(0);                
        testStatusElement.width = 25;
        testStatusElement.height = 25;

        var testStatusContext = testStatusElement.getContext('2d');
        testStatusContext.beginPath();
        testStatusContext.arc(12, 12, 10, 0, Math.PI * 2, true); 
        testStatusContext.closePath();
        testStatusContext.fillStyle = data.BuildPassed ? '#859900' : '#dc322f';
        testStatusContext.fill();
    },

    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title); 
    }
}